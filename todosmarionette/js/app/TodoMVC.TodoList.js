TodoMVC.module('TodoList', function(TodoList, App, Backbone, Marionette, $, _) {
	'use strict';

	// Router

	TodoList.Router = Marionette.AppRouter.extend({
		appRoutes: {
			'*filter':  'filterItems'
		}
	});


	// Controller

	TodoList.Controller = function() {
		this.TodoList = new App.Todos.TodoList();
	};

	_.extend(TodoList.Controller.prototype, {

		start: function() {
			this.showHeader(this.TodoList);
			this.showFooter(this.TodoList);
			this.showTodoList(this.TodoList);

			this.TodoList.fetch();
		},

		showHeader: function(todoList) {
			var header = new App.Layout.Header({
				collection: todoList
			});
			App.header.show(header);
		},

		showFooter: function(todoList) {
			var footer = new App.Layout.Footer({
				collection: todoList
			});
			App.footer.show(footer);
		},

		showTodoList: function(todoList) {
			App.main.show(new TodoList.Views.ListView({
				collection: todoList
			}));
		},

		filterItems: function(filter) {
			App.vent.trigger('todoList:filter', filter ? filter.trim() : '');
		}
	});

	// Initializer

	TodoList.addInitializer(function() {
		var controller = new TodoList.Controller();
		var router = new TodoList.Router({
			controller: controller
		});

		controller.start();
	});

});
