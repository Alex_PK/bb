var app = app || {};

(function() {
	'use strict';

	app.Book = Backbone.Model.extend(
		{
			defaults: {
				coverImage: 'img/placeholder.jpg',
				title: 'No Title',
				author: 'Unknown',
				releaseDate: 'unknown',
				keywords: 'None'
			}
		}
	);
})();
