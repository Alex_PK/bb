var app = app || {};

(function() {
	'use strict';
	app.BookView = Marionette.ItemView.extend(
		{
			tagName: 'div',
			className: 'bookContainer',
			template: '#bookTemplate',

			events: {
				'click .delete':    'deleteBook'
			},

			/*
			render: function() {
				this.$el.html( this.template( this.model.toJSON() ) );
				return this;
			},
			*/

			deleteBook: function() {
				this.model.destroy();

				this.remove();
			}
		}
	);
})();
