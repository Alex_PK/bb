<?php

class Books
{
	static protected $_instance = null;

	/** @var \MongoCollection  */
	protected $collection;

	static public function db()
	{
		if (is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	protected function __construct()
	{
		$client = new MongoClient();
		$db = $client->selectDb('bblibrary');
		$this->collection = $db->books;
	}

	public function getAll()
	{
		$res = [];

		$data = $this->collection->find()->sort(['title'=>1]);
		foreach ($data as $row) {
			$res[] = new Book($row);
		}

		return $res;
	}

	public function find($id)
	{
		if ( ! $id instanceof MongoId) {
			$id = new MongoId($id);
		}

		$data = $this->collection->find(['_id' => $id])->getNext();
		if ($data) {
			return new Book($data);

		} else {
			return null;
		}
	}

	public function save($data)
	{
		$this->collection->save($data);
		return $data;
	}

	public function delete($id)
	{
		return $this->collection->remove(['_id' => $id]);
	}
}


class Book implements JsonSerializable
{
	protected $data = [];

	public function __construct($data=[])
	{
		$this->data = [];

		foreach ($data as $k => $v) {
			if ($k == '_id' && $v instanceof MongoId) {
				$k = 'id';
				$v = (string)$v;
			}
			$this->data[$k] = $v;
		}
	}

	public function __set($k, $v)
	{
		$this->data[$k] = $v;
	}

	public function __get($k)
	{
		return $this->data[$k];
	}

	public function save()
	{
		$data = [];
		foreach ($this->data as $k => $v) {
			if ($k == 'id' && $v != '') {
				$k = '_id';
				$v = new MongoId($v);
			}

			$data[$k] = $v;
		}


		$res = Books::db()->save($data);
		$this->data['id'] = (string)$res['_id'];

		return $this;
	}

	public function delete()
	{
		Books::db()->delete(new MongoId($this->id));
	}

	public function jsonSerialize()
	{
		return $this->data;
	}

}

function process()
{
	$nameLen = mb_strlen($_SERVER['SCRIPT_NAME']);
	$req = mb_substr($_SERVER['REQUEST_URI'], $nameLen-7);  // 7 = strlen('api.php')
	$parts = mb_split('/', $req);

	$method = mb_strtolower($_SERVER['REQUEST_METHOD']);

	$call = array_shift($parts);

	$func = $method.'_'.$call;

	$res = $func($parts);

	header('Content-type: application/json');
	print json_encode($res);
	exit;
}

function get_books($params)
{
	if (array_key_exists(0, $params)) {
		// get single book
		$id = $params[0];
		return Books::db()->find($id);

	} else {
		return Books::db()->getAll();
	}
}

function post_books($params)
{
	$data = parse_request_body();

	$book = new Book($data);

	return $book->save();
}

function put_books($params)
{
	if (array_key_exists(0, $params)) {
		// get single book
		$id = $params[0];
		$book = Books::db()->find($id);
		if (!$book) {
			return ['err' => 'no data'];
		}

		$data = parse_request_body();

		foreach (array('title', 'author', 'releaseDate') as $k) {
			if (array_key_exists($k, $data)) {
				$book->$k = $data[$k];
			}
		}

		return $book->save();

	} else {
		return ['err'=>'broken'];
	}
}

function delete_books($params)
{
	$id = $params[0];
	$book = Books::db()->find($id);
	if (!$book) {
		return ['err' => 'no data', 'id' => $id];
	}

	return $book->delete();
}

function parse_request_body()
{
	return json_decode(file_get_contents('php://input'), true);
}

process();
