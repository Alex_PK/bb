define(['backbone'], function(Bb) {
	'use strict';

	var Item = Bb.Model.extend({
		defaults: {
			price: 35,
			photo: 'http://www.placedog.com/100/100'
		}
	});

	return Item;
});
