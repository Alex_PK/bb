define( ['backbone', 'models/item'], function(Bb, Item) {
	'use strict';

	var Cart = Bb.Collection.extend({
		model: Item,

		initialize: function() {
			this.on('add', this.updateSet, this);
		},

		updateSet: function() {
			var items = this.models; // WHAT??
		}
	});

	return Cart;
});
