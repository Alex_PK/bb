define( ['underscore', 'backbone', 'jquery', 'text!templates/item-template.html'], function(_, Bb, $, itemTemplate) {
	'use strict';

	var ItemView = Bb.View.extend({
		tagName: 'div',
		className: 'item-wrap',
		template: _.template( itemTemplate ),

		render: function() {
			this.$el.html(this.template(this.model.toJson()));
			return this;
		}
	});

	return ItemView;

});
