require.config({
	paths: {
		'underscore': 'lib/underscore',
		'backbone': 'lib/backbone',
		'text': 'lib/text'
	},
	shim: {
		'underscore': {
			exports: '_'
		},
		'backbone': {
			deps: ['underscore', 'jquery'],
			exports: 'Backbone'
		}
	}
});

require(['underscore'], function(_) {
	console.log(_);
});
