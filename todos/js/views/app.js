var app = app || {};

(function() {
	'use strict';
	
	app.AppView = Backbone.View.extend({
		el: '#todoapp',

		statsTemplate: _.template( $('#stats-template').html() ),

		events: {
			'keypress #new-todo':		'createOnEnter',
			'click #clear-completed':	'clearCompleted',
			'click #toggle-all':		'toggleAllComplete'
		},

		initialize: function() {
			this.allCheckbox 	= this.$('#toggle-all')[0];
			this.$input 		= this.$('#new-todo');
			this.$footer		= this.$('#footer');
			this.$main			= this.$('#main');
			
			this.listenTo(app.TodoList, 'add', this.addOne);
			this.listenTo(app.TodoList, 'reset', this.addAll);
			this.listenTo(app.TodoList, 'change:completed', this.filterOne);
			this.listenTo(app.TodoList, 'filter', this.filterAll);
			this.listenTo(app.TodoList, 'all', this.render);
			
			app.TodoList.fetch();
		},

		render: function() {
			var completed = app.TodoList.completed().length;
			var remaining = app.TodoList.remaining().length;
			
			if (app.TodoList.length) {
				this.$main.show();
				this.$footer.show();
				
				this.$footer.html(
					this.statsTemplate({
						completed: completed,
						remaining: remaining
					})
				);
				
				this.$('#filters li a')
					.removeClass('selected')
					.filter('[href="#/' + (app.TodoFilter || '' ) +'"]')
					.addClass('selected')
				;
			} else {
				this.$main.hide();
				this.$footer.hide();
			}
			
			this.allCheckbox.checked = ! remaining;
		},

		addOne: function(todo) {
			var view = new app.TodoView({ model: todo });
			$('#todo-list').append( view.render().el );
		},

		addAll: function() {
			this.$('#todo-list').html('');
			app.TodoList.each( this.addOne, this );
		},

		filterOne: function(todo) {
			todo.trigger('visible');
		},

		filterAll: function() {
			app.TodoList.each( this.filterOne, this );
		},

		newAttributes: function() {
			return {
				title: this.$input.val().trim(),
				order: app.TodoList.nextOrder(),
				completed: false
			};
		},

		createOnEnter: function(event) {
			if (event.which !== ENTER_KEY || ! this.$input.val().trim() ) {
				return;
			}
			
			app.TodoList.create( this.newAttributes() );
			this.$input.val('');
		},

		clearCompleted: function() {
			_.invoke( app.TodoList.completed(), 'destroy' );
			return false;
		},

		toggleAllCompleted: function() {
			var completed = this.allCheckbox.completed;
			
			app.TodoList.each(function(todo) {
				todo.save({
					'completed': completed
				});
			});
		}
		
	});
})();
