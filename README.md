# These are not the scripts you are looking for

This project contains my experiments with Backbone, while reading Addy Osmani's book.

## If you must find some droids... ehm... scripts

This repository contains some self made components:

- in library/api I implemented a VERY simple API component in PHP, using MongoDB as
  data storage, instead of using node.js as the book suggested

- nothing more... yet.

## License

Everything in this repository except for the parts copied from the book are (c) by Alessandro Pellizzari
and distributed under the terms of the BSD license, available in the LICENSE.md file
