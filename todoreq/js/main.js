require.config({
	paths: {
		jquery: 'lib/jquery',
		underscore: 'lib/underscore',
		backbone: 'lib/backbone',
		text: 'lib/require.text'
	}
});

require(['views/app'], function( AppView ) {
	var app_view = new AppView();
});
