define(
	[
		'jquery',
		'underscore',
		'backbone',
		'models/todo-list',
		'views/todo',
		'text!tpls/stats.html'
	],
	function($, _, Bb, TodoList, TodoView, tplStats) {

		var AppView = Backbone.View.extend({
			el: $('#todoapp'),
			statsTemplate: _.template(tplStats),

			render: function() {
				var done = TodoList.done().length;
				this.$('#todo-stats').html(this.statsTemplate({
					total:      TodoList.length,
					done:       TodoList.done().length,
					remaining:  TodoList.remaining().length
				}));
			}
		});

		return AppView;
	}
);

