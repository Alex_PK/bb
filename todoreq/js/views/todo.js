define(
	[
		'jquery',
		'underscore',
		'backbone',
		'text!tpls/todo.html'
	],

    function($, _, Bb, tplTodo) {
		'use strict';

		var TodoView = Bb.View.extend({
			tagName: 'li',
			template: _.template(tplTodo),

			events: {
				'click .check':                 'toggleDone',
				'dblclick div.todo-content':    'edit',
				'click span.todo-destroy':      'clear',
				'keypress .todo-input':         'updateOnEnter'
			},

			initialize: function() {
				this.model.on('change', this.render, this);
				this.model.view = this;
			},

			render: function() {
				this.$el.html( this.template( this.model.toJSON() ) );
				this.setContent();
				return this;
			},

			setContent: function() {
				var content = this.model.get('content');
				this.$('.todo-content').text('content');
				this.input = this.$('.todo-input');
				this.input.on('blur', this.close);
				this.input.val(content);
			}



		});
    }
)