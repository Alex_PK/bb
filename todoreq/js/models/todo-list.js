define(
	[
		'underscore',
		'backbone',
		'lib/backbone.localStorage',
		'models/todo'
	],
	function(_, Bb, Store, Todo) {
		'use strict';

		var TodoList = Bb.Collection.extend({
			model: Todo,
			localStorage: new Store('todo'),

			done: function() {
				return this.filter(function(todo) {
					return todo.get('done');
				});
			},

			remaining: function() {
				return this.without.apply(this, this.done());
			}
		});

		return TodoList;
	}
);
