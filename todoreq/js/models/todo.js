define(
	[
		'underscore',
		'backbone'
	],
	function(_, Bb) {
		'use strict';

		var TodoModel = Bb.Model.extend({
			defaults: {
				content: 'empty todo...',
				done: false
			},

			initialize: function() {

			},

			toggle: function() {
				this.save({done: ! this.get('done')});
			},

			clear: function() {
				this.destroy();
				this.view.remove();
			}

		});

		return TodoModel;
    }
);
